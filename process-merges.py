#!/usr/bin/python3
# SPDX-License-Identifier: MIT

import os
import git
import gitlab
import gitlab.v4.objects
import time


gl = gitlab.Gitlab.from_config('apertis', [
      os.path.expanduser('~/.python-gitlab.cfg'),
      # FIXME add xdg dirs
    ])

gl.auth()
group = gl.groups.get("pkg", lazy=True);

for mr in group.mergerequests.list(state = "opened", as_list = False, lazy = True):
    if mr.target_branch != "apertis/v2022dev2":
        continue
    if mr.author["username"] != "not_a_robot":
        continue

    #if mr.iid < 5:
    #    continue
    print(f"{time.strftime('%H:%M:%S')} - {mr.web_url}")

    project = gl.projects.get(mr.project_id, lazy=True)
    pmr = project.mergerequests.get(mr.iid, access_raw_diffs=True, lazy=True)

    valid = True
    for p in pmr.pipelines():
        print(f"\tPipeline {p['web_url']} - {p['status']}")
        if p["status"] != "success":
            valid = False

    if valid: 
        for d in pmr.diffs.list():
            diff = pmr.diffs.get(d.id)
            for change in diff.diffs:
                if change["new_path"] == "debian/changelog":
                    if change["diff"].find("PLEASE SUMMARIZE") >= 0:
                        print("\tPlease summarize in changelog")
                        valid = False

    print(f"Valid: {valid}!")
    if valid:
        print("Will merge!")
        pmr.merge(merge_when_pipeline_succeeds=True)
