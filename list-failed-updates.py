#!/usr/bin/python3
# SPDX-License-Identifier: MIT

import os
import git
import gitlab
import gitlab.v4.objects
import time
import sys


gl = gitlab.Gitlab.from_config('apertis', [
      os.path.expanduser('~/.python-gitlab.cfg'),
      # FIXME add xdg dirs
    ])

gl.auth()
group = gl.groups.get("pkg", lazy=True);

for p in group.projects.list(as_list = False, lazy = True):
    project = gl.projects.get(p.id, lazy=True)
    for pi in project.pipelines.list(ref="debian/bullseye", order_by = "id", per_page=1):
        if pi.status != "success":
            print(f"{p.path} - {pi.status} {pi.web_url}")
            if pi.status != "pending" and pi.status != "running" and pi.status != "waiting_for_resource":
                for j in pi.jobs.list():
                    print(f"{j.name} - {j.status}")
                    #if j.name != "merge-updates" and j.status != "failed":
                    #    print("CREATING new pipeline")
                    #   #project.pipelines.create({'ref': 'debian/bullseye'})
                    #   #sys.exit(0)
        break

    #print(dir(project.branches()))
